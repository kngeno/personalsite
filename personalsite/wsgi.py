"""
WSGI config for personalsite project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""
import os
import sys
import site
from django.core.wsgi import get_wsgi_application
from mezzanine.utils.conf import real_project_name



envpath = '/home/venv/personalsite/local/lib/python2.7/site-packages'
#envpath2 = '/home/mamase/venv/geonodems/lib/python2.7/site-packages'
# we add currently directory to path and change to it
pwd = os.path.dirname(os.path.abspath(__file__))
os.chdir(pwd)
sys.path = [pwd] + sys.path

# Append paths
site.addsitedir(envpath)
#site.addsitedir(envpath2)
sys.path.append('/home/personalsite')
sys.path.append('/home/personalsite/personalsite')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "personalsite.settings")
activate_env=os.path.expanduser("/home/venv/personalsite/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))
# This application object is used by any WSGI server configured to use this
# file. This includes Django's development server, if the WSGI_APPLICATION
# setting points here.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()